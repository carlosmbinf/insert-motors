var express = require('express');
var cookieParser = require('cookie-parser')
var app = express();
app.use(cookieParser());
require('dotenv').config({path:'./variables.env'})

// Estructura del html
var header = require('./client/header.js');
var main = require('./client/main.js');
var footer = require('./client/footer.js');

const hostname = '127.0.0.1';
const port = process.env.PORT || 3000;

var fs = require('fs');
app.use('/css', express.static(__dirname + '/client/css'));
app.use('/img', express.static(__dirname + '/client/img'));
app.use('/js', express.static(__dirname + '/client/js'));
// app.use('/font', express.static(__dirname + '/client/font'));

app.get('/', function (req, res) {
  var CT = require('./client/js/motorGT.js')

  if (req.query.ln == 'en') {    
    CT.ABE.Settings.language = 'EN'

    var ct = JSON.stringify(CT).toString();
  
  res.write(header('en',1));
  res.write(main( 1 ));
  res.write(footer('en',1));
  // res.sendFile(__dirname + '/client/index.html');
  res.end();

  }else{    
    CT.ABE.Settings.language = 'ES'

    var ct = JSON.stringify(CT).toString();
    
    
  res.write(header('es',1));
  res.write(main( 1 ));
  res.write(footer('es',1));
  // res.sendFile(__dirname + '/client/index.html');
  res.end();

  }
  
  
});

app.get('/hoteles', function (req, res) {
  // res.sendFile(__dirname + '/client/index2.html');
  var CT = require('./client/js/motorCAR.js')
  var ct = ''
  if (req.query.ln == 'en') {
    CT.ABE.Settings.language = 'EN'
    ct = JSON.stringify(CT).toString();
    res.write(header('en',2));
    res.write(main( 2 ));
    res.write(footer('en',2));
    res.end();

  }else{
    CT.ABE.Settings.language = 'ES'
    ct = JSON.stringify(CT).toString();
    res.write(header('es'));
    res.write(main( 2 ));
    res.write(footer('es'));
    res.end();
  
  }

});

app.get('/maps', function (req, res) {
  // res.sendFile(__dirname + '/client/index2.html');
  var CT = require('./client/js/motorCAR.js')
  var ct = ''
  if (req.query.ln == 'en') {
    CT.ABE.Settings.language = 'EN'
    ct = JSON.stringify(CT).toString();
    res.write(header('en',2));
    res.write(main( 3 ));
    res.write(footer('en',2));
    res.end();

  }else{
    CT.ABE.Settings.language = 'ES'
    ct = JSON.stringify(CT).toString();
    res.write(header('es'));
    res.write(main( 3 ));
    res.write(footer('es'));
    res.end();
  
  }

});

app.get('/calendar', function (req, res) {
  // res.sendFile(__dirname + '/client/index2.html');
  var CT = require('./client/js/motorCAR.js')
  var ct = ''
  if (req.query.ln == 'en') {
    CT.ABE.Settings.language = 'EN'
    ct = JSON.stringify(CT).toString();
    res.write(header('en',2));
    res.write(main( 4 ));
    res.write(footer('en',2));
    res.end();

  }else{
    CT.ABE.Settings.language = 'ES'
    ct = JSON.stringify(CT).toString();
    res.write(header('es'));
    res.write(main( 4 ));
    res.write(footer('es'));
    res.end();
  
  }

});

app.get('/schedule', function (req, res) {
  // res.sendFile(__dirname + '/client/index2.html');
  var CT = require('./client/js/motorCAR.js')
  var ct = ''
  if (req.query.ln == 'en') {
    CT.ABE.Settings.language = 'EN'
    ct = JSON.stringify(CT).toString();
    res.write(header('en',2));
    res.write(main( 5 ));
    res.write(footer('en',2));
    res.end();

  }else{
    CT.ABE.Settings.language = 'ES'
    ct = JSON.stringify(CT).toString();
    res.write(header('es'));
    res.write(main( 5 ));
    res.write(footer('es'));
    res.end();
  
  }

});

app.get('/chansey', function (req, res) {
  // res.sendFile(__dirname + '/client/index2.html');
  var CT = require('./client/js/motorCAR.js')
  var ct = ''
  if (req.query.ln == 'en') {
    CT.ABE.Settings.language = 'EN'
    ct = JSON.stringify(CT).toString();
    res.write(header('en',2));
    res.write(main( 6 ));
    res.write(footer('en',2));
    res.end();

  }else{
    CT.ABE.Settings.language = 'ES'
    ct = JSON.stringify(CT).toString();
    res.write(header('es'));
    res.write(main( 6 ));
    res.write(footer('es'));
    res.end();
  
  }

});

app.get('/weedle', function (req, res) {
  // res.sendFile(__dirname + '/client/index2.html');
  var CT = require('./client/js/motorCAR.js')
  var ct = ''
  if (req.query.ln == 'en') {
    CT.ABE.Settings.language = 'EN'
    ct = JSON.stringify(CT).toString();
    res.write(header('en',2));
    res.write(main( 7 ));
    res.write(footer('en',2));
    res.end();

  }else{
    CT.ABE.Settings.language = 'ES'
    ct = JSON.stringify(CT).toString();
    res.write(header('es'));
    res.write(main( 7 ));
    res.write(footer('es'));
    res.end();
  
  }

});

app.get('/blissey', function (req, res) {
  // res.sendFile(__dirname + '/client/index2.html');
  var CT = require('./client/js/motorCAR.js')
  var ct = ''
  if (req.query.ln == 'en') {
    CT.ABE.Settings.language = 'EN'
    ct = JSON.stringify(CT).toString();
    res.write(header('en',2));
    res.write(main( 8 ));
    res.write(footer('en',2));
    res.end();

  }else{
    CT.ABE.Settings.language = 'ES'
    ct = JSON.stringify(CT).toString();
    res.write(header('es'));
    res.write(main( 8 ));
    res.write(footer('es'));
    res.end();
  
  }

});

app.get('/ducklett', function (req, res) {
  // res.sendFile(__dirname + '/client/index2.html');
  var CT = require('./client/js/motorCAR.js')
  var ct = ''
  if (req.query.ln == 'en') {
    CT.ABE.Settings.language = 'EN'
    ct = JSON.stringify(CT).toString();
    res.write(header('en',2));
    res.write(main( 9 ));
    res.write(footer('en',2));
    res.end();

  }else{
    CT.ABE.Settings.language = 'ES'
    ct = JSON.stringify(CT).toString();
    res.write(header('es'));
    res.write(main( 9 ));
    res.write(footer('es'));
    res.end();
  
  }

});

//   if(req.url === '/'){
//   fs.createReadStream(__dirname + '/client/index.html').pipe(res);
//   }

//  else if(req.url === '/version'){

//     res.writeHead(200,{'Content-Type': 'application/json'});
//     var obj = {
//       firstname: 'Carlos',
//       lastname: 'Medina'
//     }
//     res.end(JSON.stringify(obj));
//   }else{
//     res.writeHead(404)
//     res.end();
//   }

 

// server.listen(port, hostname, () => {
//   console.log(`Server running at http://${hostname}:${port}/`);
// });

app.listen(port, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
  })